const express = require('express')
const app = express()
const port = 3000
const path = require("path");

app.use('/public', express.static('frontend'))

app.get('/', function(req, res) {
    res.sendFile(path.join(path.dirname(__dirname) + '/frontend/html/index.html'));
});

app.listen(port, () => console.log(`ColorPickApp is running at localhost:${port}`))