const Pick = function Pick(pickedColorHex, ordinal) {
	this.hex = pickedColorHex;
	this.id = utils.generateUniqueID();
	this.pickedAt = new Date();
	this.counter = 0;
	this.ordinal = ordinal;
};

Pick.prototype.increment = function() {
	this.counter += 1;
	return this.counter;
};

Pick.createOrGet = function(pickedColorHex) {
	let instance = colorApp.picks[pickedColorHex];
	if (!instance) {
		instance = new Pick(pickedColorHex, colorApp.history.length);
		colorApp.picks[pickedColorHex] = instance;
	}

	return instance;
};
