const utils = {
	randomColor: function() {
		return "#" + utils.fullColorHex(...Array.from(Array(3), utils.randomColorChannel));
	},

	rgbToHex: function(rgb) {
		const hex = Number(rgb).toString(16);
		return hex.length < 2 ? "0" + hex : hex;
	},

	fullColorHex: function(...rgb) {
		return rgb.map(this.rgbToHex).reduce((sum, cur) => sum + cur);
	},

	randomColorChannel: function() {
		return parseInt(Math.random() * 255);
	},

	generateUniqueID: function() {
		return Math.random()
			.toString(36)
			.substr(2);
	},

	getCurrentTimeDate: () => {
		const time = new Date();
		return [time.toDateString(), time.toTimeString().split(" ")[0]]
	}
};
