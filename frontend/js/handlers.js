function changeColor() {
	if (!this.pick instanceof Pick) {
		throw "Context must be set to a Pick instance";
	}

	colorApp.$colorInput.value = this.pick.hex;
	handleColorPickChange.call(colorApp.$colorInput);
}

function handleColorPickChange() {
	// Store previous pick in history
	colorApp.history.push(this.previousValue);

	// Try to get or create pick by current value
	const pick = Pick.createOrGet(this.value);
	pick.increment();

	// Modify dom based on new pick
	colorApp.pickDomWriter(pick, this.previousValue);
	// store current value as previous
	this.previousValue = pick;
}

{
	const $historyContainer = document.querySelector("#history > .items");
	function emptyHistory() {
		$historyContainer.innerHTML = "";
	}
}
