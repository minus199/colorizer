function eventBubble() {
				// The last argument that is passed here to addEventListener is called useCapture.
				// The default value is false(Meaning, if we do not pass any value, the value will be set to false)
				// false means that the "capture" is disabled
				
				//Events bubbles up in the dom
				// So if we click on the #debugger div, it also means we clicked the #app div and therefore the body element
				// When the pointer is on a certain position in the screen, ask yourself - what elements are bellow the cursor?
				// Then, look at the html. The deeper the element node is in the html the structure, the closest it is to the curosr, and will be handling the event before the rest.

				//useCapture means that the event will first be sent to that element, before reaching the rest

				// if more than 2 elements should be triggered, and they both useCapture, than the capture will happen by the order of registration

				document.body.addEventListener("click", function() { // First to register
					console.log("body click");
				}, true);

				document.querySelector("#app").addEventListener("click", function() { // Second to register
					console.log("app click");
				});

				document.querySelector("#debugger").addEventListener("click", function() { // Third to register
					console.log("debugger click");
				}, true);

				document.querySelector("#history").addEventListener("click", function() { // .....
					console.log("history click");
				});
			}