// Init the app
const colorApp = { $colorInput: null, picks: {}, history: [], pickDomWriter: null };

// Continue init after dom has fully loaded
document.addEventListener("DOMContentLoaded", function(event) {
	const $historyContainer = document.querySelector("#history");
	const $historyItemsContainer = $historyContainer.querySelector(".items");

	// The main input for this app - the color picker.
	const $colorInput = (colorApp.$colorInput = document.querySelector("#main-color-pick"));
	// Load the initial value of this element as the first pick 
	const initialPick = ($colorInput.previousValue = Pick.createOrGet($colorInput.value));

	// Place holder elements for color pick stats
	const $selectedColor = document.querySelector("#selected-color");
	const $previousColor = document.querySelector("#previous-selected-color");
	const $pickCount = document.querySelector("#num-picked");
	const $pickID = document.querySelector("#pick-id");

	// DomWriters takes a pick, creates an html element and injects it to the proper place in the dom.
	const colorPickDomWriter = (function() {
		// Nested function -- updates the selected elements with data from 2 picks
		return function writer(currentPick, previousPick) {
			if (currentPick instanceof Pick) {
				$selectedColor.innerText = currentPick.hex;
				$selectedColor.style["box-shadow"] = `2px 4px 16px 0px ${currentPick.hex}`;

				$selectedColor.style.background = `linear-gradient(180deg, #00000000 0%, ${currentPick.hex + "42"} 100%)`;
				$pickCount.innerText = currentPick.counter;
				$pickID.innerText = currentPick.id;
			}

			if (previousPick instanceof Pick) {
				$previousColor.innerText = previousPick.hex;
				$previousColor.style.background = `linear-gradient(45deg, #00000000 0%, ${previousPick.hex + "42"} 100%)`;
				$previousColor.style["box-shadow"] = `2px 4px 16px 0px ${previousPick.hex}`;
			}
		};
	})();

	const colorPickHistoryItemDomWriter = (function historyWriter() {
		return function writer(pick) {
			// create a new element for this pick and put inside history container
			const $pickHistory = document.createElement("div");
			$pickHistory.pick = pick;
			$pickHistory.addEventListener("click", changeColor);

			// prepare the history item
			$pickHistory.style["background-color"] = pick.hex;
			const [currentDate, currentTime] = utils.getCurrentTimeDate();

			$pickHistory.title = `Your #${pick.ordinal} pick was ${pick.hex} at ${currentDate} on ${currentTime}`;
			$pickHistory.classList.add("item", "history-item");

			$historyItemsContainer.prepend($pickHistory);

			// update the total history items count
			// this will update the data-before attribute on #history
			// see the css for div#history::before (we can get a value from an html attribute in css by using attr(attrName))
			$historyContainer.dataset["before"] = colorApp.history.length;
			return true;
		};
	})();

	// Exposes the writing operation
	colorApp.pickDomWriter = function(current, previous) {
		colorPickDomWriter(current, previous);
		colorPickHistoryItemDomWriter(current);
	};

	// Query the dom and store into $colorInput
	// Register event handlers
	// Load initial value as last pick(history)
	// Show the element only after ready
	$colorInput.addEventListener("input", (...args) => console.log("input", args), false);
	$colorInput.addEventListener("change", handleColorPickChange, false);
	$colorInput.classList.remove("hidden");

	colorApp.pickDomWriter(initialPick, null);
});
