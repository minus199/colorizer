{
	const getDebugOptions = (function() {
		const $times = document.querySelector("#simulate-num-picks");
		const $timesLabel = document.querySelector("label[for='simulate-num-picks']");
		const $interval = document.querySelector("#simulate-pick-interval");
		const $intervalLabel = document.querySelector("label[for='simulate-pick-interval']");
		
		const $debugger = document.querySelector("#debugger");

		function valueObserver($el, $elLabel, normFunc = strVal => strVal) {
			function apply() {
				$elLabel.querySelector(".label-value").innerText = normFunc(this.value);
			}

			$el.addEventListener("input", apply, false);
			apply.call($el);
		}

		// Updates the value from the range inputs
		valueObserver($times, $timesLabel);
		valueObserver($interval, $intervalLabel, val => `${Number((val | 0) / 1000).toFixed(2)} Sec.`);
	
		document.querySelector("#enable-debug").addEventListener("change", function() {
			this.checked ? $debugger.classList.remove("hidden-block") : $debugger.classList.add("hidden-block");
		});

		return function() {
			// fetchs the values from the debugging inputs
			return { times: Number($times.value), interval: Number($interval.value)};
		};
	})();

	{
		let inProgress = false;
		let opts;
	
		function blockingRandomize() {
			const start = Date.now();
			while (Date.now() - start < 1000) {
				// console.log(".");
			}
		}

		// What would happen if we would have used a loop instead of setTimeout recursion?
		function randomize(current = 1) {
			if (current === 1) {
				if (inProgress === true) {
					throw new RandomizeInProgressException();
				}

				opts = getDebugOptions(current);
			}

			setTimeout(() => {
				inProgress = true;

				const color = utils.randomColor();
				changeColor.call({ pick: Pick.createOrGet(color) });
				if (current > opts.times) {
					inProgress = false;
					return;
				}

				randomize(current + 1);
			}, opts.interval);
		}
	}
}
